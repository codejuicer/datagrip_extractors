/**
 * Nested JSON export script for JetBrains database output.
 * This creates a valid JSON file that is structured by column names.
 *
 * A column name forms a simple JSON path. Each string is separated by a period.
 * Example:
 *   "First.Second" would generate {"First": {"Second": value}}
 *
 * Arrays can be added as well using [] in the column name.
 * NOTE: currently an array cannot directly follow an array. (i.e. array1[].array2[])
 *       But, an array can follow ano object in an array. (i.e. array1[].nextobj.array2[])
 *
 * Example:
 *   Assume the following are parsed together
 *    "First.Second[]" -> {"First": { "Second": [value] }}
 *    "First.Second[]" -> {"First": { "Second": [value, value_2]}}
 *    "First.Second[].Third" -> {"First": { "Second": [ value, value2, {"Three": value3} ] } }
 *
 * @author Cole Kelley
 */

// -----------------------------------------------------------------------------
// -- Script settings
// -----------------------------------------------------------------------------
var IGNORE_NULLS = true; // Will not add null values to the JSON output.

// -----------------------------------------------------------------------------
// -- Support functions
// -----------------------------------------------------------------------------

/**
 * Generates a JSON namespace from a string. Quite a simple one.
 * Much more could be done to make it better.
 * @param namespace_string
 * @return {{branches: [], leaf: *}}
 */
function parseNamespace(namespace_string) {

    var names = namespace_string.split('.');
    var structure = {branches: [], leaf: names.pop()};

    for(var branch in names) {
        var obj_type = "object";
        if(names[branch].endsWith('[]')) {
            names[branch] = names[branch].replace('[]', '');
            obj_type = "array";
        }

        structure.branches.push({"name": names[branch], "type": obj_type});
    }

    return structure;
}

/**
 * Handles adding values to leaves of the structures.
 * @param name Name of the leaf
 * @param the_object Object to put the value into.
 * @param the_value The actual value.
 * @return {*}
 */
function processLeaf(name, the_object, the_value) {
    if(Array.isArray(the_object)) {
        //the_object.push(the_value)
        if(the_object.length === 0) {
            var o = {}
            o[name] = the_value
            the_object.push(o)
        }
        else {
            // Always update the last object
            the_object[the_object.length-1][name] = the_value
        }
    }
    else {
        if(name.endsWith('[]')) {
            name = name.replace('[]','')
            if(!Array.isArray(the_object[name]))
                the_object[name] = []

            the_object[name].push(the_value)
        }
        else the_object[name] = the_value;
    }
    return the_object;
}

/**
 * Builds a JSON document from a given namespace.
 * Nesting of objects within arrays is not working correctly yet.
 * @param namespace
 * @param the_object
 * @param the_value
 * @return {*}
 */
function buildJSON(namespace, the_object, the_value) {

    // No branches left. Process the leaf node.
    if(namespace.branches.length === 0)
        return processLeaf(namespace.leaf, the_object, the_value)

    var branch = namespace.branches.shift()

    if(branch.type === 'array') {
        if(the_object[branch.name] === undefined)
            the_object[branch.name] = []

        buildJSON(namespace, the_object[branch.name], the_value)

        return the_object
    }

    // Handling of object types.
    if(the_object[branch.name] === undefined)
        the_object[branch.name] = {}

    if(Array.isArray(the_object)) {
        // Namespace needs the current branch back for proper nesting.
        namespace.branches.unshift(branch)
        if(the_object.length === 0)
            the_object.push(buildJSON(namespace, the_object[branch.name], the_value))
        else {
            buildJSON(namespace, the_object[the_object.length - 1], the_value)
        }
    }
    else {
        buildJSON(namespace, the_object[branch.name], the_value)
    }

    return the_object
}

// ---------------------------------------------------------------------------------------
// -- DataGrip processing
// ---------------------------------------------------------------------------------------
function output() {
    for (var i = 0; i < arguments.length; i++) {
        OUT.append(arguments[i]);
    }
}

function mapEach(iterable, f) {
    var vs = [];
    eachWithIdx(iterable, function (i) {
            vs.push(f(i))
            ;
        }
    );
    return vs;
}

function eachWithIdx(iterable, f) {
    var i = iterable.iterator();
    var idx = 0;

    while (i.hasNext())
        f(i.next(), idx++);
}

var the_data = []

eachWithIdx(ROWS, function (row) {
    data = the_data;
    var row_data = {};

    mapEach(COLUMNS, function (col) {
        if(IGNORE_NULLS && FORMATTER.format(row, col) === "NULL")
            return;

        var value = undefined;

        switch(FORMATTER.getTypeName(row, col)) {
            case 'bigint':
            case 'smallint':
            case 'int':
            case 'tinyint':
                value = parseInt(FORMATTER.format(row, col))
                break;

            case 'datetime':
                value = new Date(FORMATTER.format(row, col))
                break;

            case 'bit':
            case 'bool':
            case 'boolean':
                value = FORMATTER.format(row, col) === 'true'
                break;

            default:
                value = FORMATTER.format(row, col)

        }

        buildJSON(parseNamespace(col.name()), row_data, value);
    })

    data.push(row_data);
});

output(JSON.stringify(the_data, null, 2));
